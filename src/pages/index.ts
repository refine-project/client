import AgentProfile from './agent-profile';
import Agents from './agents';
import MyProfile from './my-profile';
import PropertyDetails from './property-details';
import AllProperties from './all-properties';
import CreateProperties from './create-propertie';
import EditProperty from './edit-property';
import { Login } from './login';
import Home from './home';

export {
    AgentProfile,
    Agents,
    MyProfile,
    PropertyDetails,
    AllProperties,
    CreateProperties,
    EditProperty,
    Login,
    Home,
};